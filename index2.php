<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new animal("Shaun");
echo "Name  : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->coldblood . "<br><br>";

$sungokong = new ape("Kera Sakti");
echo "Name  : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->coldblood ."<br>";
echo "yell : " . $sungokong->yell . "<br><br>";
$sungokong->yell();

$kodok = new frog("Buduk");
echo "Name  : " . $kodok->name . "<br>";
echo "Legs : " . $kodok ->legs . "<br>";
echo "Cold Blooded : " . $kodok->coldblood . "<br>";
echo "Jump : " . $kodok->jump . "<br><br>";
$kodok->Jump();
?>