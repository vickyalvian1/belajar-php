<?php
function ubah_huruf($string){
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $output = "";
    for($i =0; $i < strlen($string); $i++) {
        $position = strpos($abjad, $string[$i]);
        $output .= substr($abjad, $position + 1, 1);
    }
    return $output . "<br>";
}

echo ubah_huruf('wow');
echo ubah_huruf('developer');
echo ubah_huruf('laravel');
echo ubah_huruf('keren');
echo ubah_huruf('semangat');
